package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectRemoveByIdRequest;
import t1.dkhrunina.tm.dto.response.project.ProjectRemoveByIdResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[Remove project by id]");
        System.out.println("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        @NotNull final ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}