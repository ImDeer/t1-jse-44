package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskRemoveByIndexRequest;
import t1.dkhrunina.tm.dto.response.task.TaskRemoveByIndexResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[Remove task by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken(), index);
        @NotNull final TaskRemoveByIndexResponse response = getTaskEndpoint().removeTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}