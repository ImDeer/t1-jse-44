package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectStartByIndexRequest;
import t1.dkhrunina.tm.dto.response.project.ProjectStartByIndexResponse;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start project by index.";

    @Override
    public void execute() {
        System.out.println("[Start project by index]");
        System.out.println("Enter index: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken(), index);
        @NotNull final ProjectStartByIndexResponse response = getProjectEndpoint().startProjectByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}