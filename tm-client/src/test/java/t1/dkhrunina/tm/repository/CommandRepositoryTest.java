package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.repository.ICommandRepository;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.command.system.ApplicationAboutCommand;
import t1.dkhrunina.tm.command.system.ApplicationVersionCommand;
import t1.dkhrunina.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitCategory.class)
public class CommandRepositoryTest {

    @NotNull
    private ICommandRepository commandRepository;

    @NotNull
    private AbstractCommand testCommand;

    @Before
    public void initRepository() {
        commandRepository = new CommandRepository();
        testCommand = new ApplicationAboutCommand();
        commandRepository.add(testCommand);
    }

    @Test
    public void testAdd() {
        @NotNull final AbstractCommand command = new ApplicationVersionCommand();
        commandRepository.add(command);
        @Nullable final AbstractCommand newCommand = commandRepository.getCommandByName(command.getName());
        Assert.assertNotNull(newCommand);
        Assert.assertEquals(command, newCommand);
    }

    @Test
    public void testAddNull() {
        int numberOfTerminalCommands = commandRepository.getTerminalCommands().size();
        commandRepository.add(null);
        Assert.assertEquals(numberOfTerminalCommands, commandRepository.getTerminalCommands().size());
    }

    @Test
    public void testGetCommandByArgument() {
        @Nullable final String argument = testCommand.getArgument();
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByArgument(argument);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    public void testGetCommandByArgumentNull() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByArgument(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandByArgumentEmpty() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByArgument("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandByName() {
        @Nullable final String name = testCommand.getName();
        Assert.assertNotNull(name);
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByName(name);
        Assert.assertNotNull(commandFromRepository);
        Assert.assertEquals(testCommand, commandFromRepository);
    }

    @Test
    public void testGetCommandByNameNull() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByName(null);
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandByNameEmpty() {
        @Nullable final AbstractCommand commandFromRepository = commandRepository.getCommandByName("");
        Assert.assertNull(commandFromRepository);
    }

    @Test
    public void testGetCommandsWithArgument() {
        @NotNull final Iterable<AbstractCommand> commands = commandRepository.getCommandsWithArgument();
        @NotNull final List<AbstractCommand> commandList = new ArrayList<>();
        for(AbstractCommand command : commands) commandList.add(command);
        Assert.assertTrue(commandList.size() > 0);
    }

    @Test
    public void testGetTerminalCommands() {
        @NotNull final Collection<AbstractCommand> commands = commandRepository.getTerminalCommands();
        Assert.assertTrue(commands.size() > 0);
    }

}
