package t1.dkhrunina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.endpoint.IAuthEndpoint;
import t1.dkhrunina.tm.api.endpoint.IProjectEndpoint;
import t1.dkhrunina.tm.api.endpoint.ITaskEndpoint;
import t1.dkhrunina.tm.api.endpoint.IUserEndpoint;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.request.project.ProjectClearRequest;
import t1.dkhrunina.tm.dto.request.project.ProjectCreateRequest;
import t1.dkhrunina.tm.dto.request.task.*;
import t1.dkhrunina.tm.dto.request.user.UserLoginRequest;
import t1.dkhrunina.tm.dto.request.user.UserRegisterRequest;
import t1.dkhrunina.tm.dto.request.user.UserRemoveRequest;
import t1.dkhrunina.tm.dto.response.task.TaskCreateResponse;
import t1.dkhrunina.tm.dto.response.task.TaskFindOneByIndexResponse;
import t1.dkhrunina.tm.dto.response.task.TaskListByProjectIdResponse;
import t1.dkhrunina.tm.dto.response.task.TaskListResponse;
import t1.dkhrunina.tm.dto.response.user.UserLoginResponse;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.marker.SoapCategory;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static IUserEndpoint userEndpoint;

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @NotNull
    private String projectId;

    @NotNull
    private String firstTaskId;

    @NotNull
    private String secondTaskId;

    @BeforeClass
    public static void initUser() {
        @NotNull IPropertyService propertyService = new PropertyService();
        projectEndpoint = IProjectEndpoint.newInstance(propertyService);
        taskEndpoint = ITaskEndpoint.newInstance(propertyService);
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        userEndpoint = IUserEndpoint.newInstance(propertyService);
        @NotNull UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();

        @NotNull final UserRegisterRequest registryRequest = new UserRegisterRequest("test", "test", "test@test.test");
        userEndpoint.registerUser(registryRequest);
        loginRequest = new UserLoginRequest("test", "test");
        response = authEndpoint.loginUser(loginRequest);
        userToken = response.getToken();
    }

    @AfterClass
    public static void dropUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, "test");
        userEndpoint.removeUser(removeRequest);
    }

    @Before
    public void initData() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken, "project1", "desc1");
        projectId = projectEndpoint.createProject(createRequest).getProject().getId();
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, "task1", "desc1");
        @Nullable final TaskDTO firstTask = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(firstTask);
        firstTaskId = firstTask.getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(userToken, projectId, firstTaskId);
        taskEndpoint.bindTaskToProject(bindRequest);
        taskCreateRequest = new TaskCreateRequest(userToken, "task2", "desc2");
        @Nullable final TaskDTO secondTask = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(secondTask);
        secondTaskId = secondTask.getId();
        @NotNull final TaskBindToProjectRequest bindRequest2 = new TaskBindToProjectRequest(userToken, projectId, secondTaskId);
        taskEndpoint.bindTaskToProject(bindRequest2);
    }

    @After
    public void dropData() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(clearRequest);
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(request);
    }

    @Nullable
    private TaskDTO findTaskById(@NotNull final String taskId) {
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(userToken, taskId);
        return taskEndpoint.findTaskById(request).getTask();
    }

    @Nullable
    private TaskDTO findTaskByIndex(@NotNull final Integer index) {
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(userToken, index);
        return taskEndpoint.findTaskByIndex(request).getTask();
    }

    @Test
    public void testBindToProject() {
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, "new task", "new desc");
        @NotNull final String taskId = taskEndpoint.createTask(taskCreateRequest).getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken, projectId, taskId);
        Assert.assertNotNull(taskEndpoint.bindTaskToProject(request));
        @Nullable final TaskDTO boundTask = findTaskById(taskId);
        Assert.assertNotNull(boundTask);
        Assert.assertEquals(boundTask.getProjectId(), projectId);
    }

    @Test
    public void testBindToProjectTokenNull() {
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, "new task", "new desc");
        @NotNull final String taskId = taskEndpoint.createTask(taskCreateRequest).getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(null, projectId, taskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken, firstTaskId, status);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusById(request));
        @Nullable final TaskDTO task = findTaskById(firstTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void testChangeStatusByIdTokenNull() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(null, firstTaskId, Status.IN_PROGRESS);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken, 1, status);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusByIndex(request));
        @Nullable final TaskDTO task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void testChangeStatusByIndexTokenNull() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusByIndex(request));
    }

    @Test
    public void testClear() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(taskEndpoint.clearTask(request));
        @NotNull final TaskListRequest listRequest = new TaskListRequest(userToken, Sort.BY_NAME);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(listRequest).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testClearTokenNull() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(request));
    }

    @Test
    public void testCompleteById() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken, secondTaskId);
        Assert.assertNotNull(taskEndpoint.completeTaskById(request));
        @Nullable final TaskDTO task = findTaskById(secondTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void testCompleteByIdTokenNull() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(null, secondTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(request));
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken, 1);
        Assert.assertNotNull(taskEndpoint.completeTaskByIndex(request));
        @Nullable final TaskDTO task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void testCompleteByIndexTokenNull() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskByIndex(request));
    }

    @Test
    public void testCreate() {
        @NotNull final String name = "new name";
        @NotNull final String desc = "new desc";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(userToken, name, desc);
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        @Nullable final TaskDTO returnedTask = response.getTask();
        Assert.assertNotNull(returnedTask);
        @NotNull final String taskId = returnedTask.getId();
        @Nullable final TaskDTO createdTask = findTaskById(taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(desc, createdTask.getDescription());
    }

    @Test
    public void testCreateTokenNull() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(null, "name", "desc");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void testFindOneById() {
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(userToken, firstTaskId);
        @Nullable final TaskDTO task = taskEndpoint.findTaskById(request).getTask();
        Assert.assertNotNull(task);
    }

    @Test
    public void testFindOneByIdTokenNull() {
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(null, firstTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(request));
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(userToken, 1);
        @NotNull TaskFindOneByIndexResponse response = taskEndpoint.findTaskByIndex(request);
        @Nullable final TaskDTO task = response.getTask();
        Assert.assertNotNull(task);
    }

    @Test
    public void testFindOneByIndexTokenNull() {
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskByIndex(request));
    }

    @Test
    public void testList() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken, null);
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        Assert.assertNotNull(response);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        Assert.assertEquals(firstTaskId, tasks.get(1).getId());
        Assert.assertEquals(secondTaskId, tasks.get(0).getId());
    }

    @Test
    public void testListTokenNull() {
        @NotNull final TaskListRequest request = new TaskListRequest(null, null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(request));
    }

    @Test
    public void testListByProjectId() {
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(userToken, projectId);
        @Nullable final TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(request);
        Assert.assertNotNull(response);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void testListByProjectIdTokenNull() {
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(null, projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProjectId(request));
    }

    @Test
    public void testRemoveById() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken, firstTaskId);
        Assert.assertNotNull(taskEndpoint.removeTaskById(request));
        @Nullable final TaskDTO task = findTaskById(firstTaskId);
        Assert.assertNull(task);
    }

    @Test
    public void testRemoveByIdTokenNull() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(null, firstTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(request));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken, 1);
        Assert.assertNotNull(taskEndpoint.removeTaskByIndex(request));
        @Nullable final TaskDTO task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertNotEquals(firstTaskId, task.getId());
    }

    @Test
    public void testRemoveByIndexTokenNull() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(request));
    }

    @Test
    public void testStartById() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken, secondTaskId);
        Assert.assertNotNull(taskEndpoint.startTaskById(request));
        @Nullable final TaskDTO task = findTaskById(secondTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void testStartByIdTokenNull() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(null, secondTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(request));
    }

    @Test
    public void testStartByIndex() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken, 1);
        Assert.assertNotNull(taskEndpoint.startTaskByIndex(request));
        @Nullable final TaskDTO task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void testStartByIndexTokenNull() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(null, 1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskByIndex(request));
    }

    @Test
    public void testUnbindFromProject() {
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken, projectId, firstTaskId);
        Assert.assertNotNull(taskEndpoint.unbindTaskFromProject(request));
        @Nullable final TaskDTO unboundTask = findTaskById(firstTaskId);
        Assert.assertNotNull(unboundTask);
        Assert.assertNotEquals(unboundTask.getProjectId(), projectId);
    }

    @Test
    public void testUpdateById() {
        @NotNull final String name = "NAME TEST UPDATE";
        @NotNull final String desc = "DESC TEST UPDATE";
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken, firstTaskId, name, desc);
        Assert.assertNotNull(taskEndpoint.updateTaskById(request));
        @Nullable final TaskDTO updatedTask = findTaskById(firstTaskId);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(name, updatedTask.getName());
        Assert.assertEquals(desc, updatedTask.getDescription());
    }

    @Test
    public void testUpdateByIdTokenNull() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(null, firstTaskId, "name", "desc");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String name = "NAME TEST UPDATE";
        @NotNull final String desc = "DESC TEST UPDATE";
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken, 1, name, desc);
        Assert.assertNotNull(taskEndpoint.updateTaskByIndex(request));
        @Nullable final TaskDTO updatedTask = findTaskByIndex(1);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(name, updatedTask.getName());
        Assert.assertEquals(desc, updatedTask.getDescription());
    }

    @Test
    public void testUpdateByIndexTokenNull() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(null, 1, "name", "desc");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(request));
    }

}