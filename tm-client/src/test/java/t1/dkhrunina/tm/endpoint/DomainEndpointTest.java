package t1.dkhrunina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.endpoint.IAuthEndpoint;
import t1.dkhrunina.tm.api.endpoint.IDomainEndpoint;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.request.data.*;
import t1.dkhrunina.tm.dto.request.user.UserLoginRequest;
import t1.dkhrunina.tm.dto.response.user.UserLoginResponse;
import t1.dkhrunina.tm.marker.SoapCategory;
import t1.dkhrunina.tm.service.PropertyService;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @NotNull
    private static IDomainEndpoint domainEndpoint;

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void initSettings() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        domainEndpoint = IDomainEndpoint.newInstance(propertyService);
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull final UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();
    }

    @Test
    public void TestBackup() {
        @NotNull final DataBackupSaveRequest backupSaveRequest = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveDataBackup(backupSaveRequest));
        @NotNull final DataBackupLoadRequest backupLoadRequest = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadDataBackup(backupLoadRequest));
    }

    @Test
    public void TestBase64() {
        @NotNull final DataSaveBase64Request saveBase64Request = new DataSaveBase64Request(adminToken);
        Assert.assertNotNull(domainEndpoint.saveBase64Data(saveBase64Request));
        @NotNull final DataLoadBase64Request loadBase64Request = new DataLoadBase64Request(adminToken);
        Assert.assertNotNull(domainEndpoint.loadBase64Data(loadBase64Request));
    }

    @Test
    public void TestBinary() {
        @NotNull final DataSaveBinaryRequest saveBinaryRequest = new DataSaveBinaryRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveBinaryData(saveBinaryRequest));
        final DataLoadBinaryRequest loadBinaryRequest = new DataLoadBinaryRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadBinaryData(loadBinaryRequest));
    }

    @Test
    public void TestJsonFasterXML() {
        @NotNull final DataSaveJsonFasterXmlRequest saveJsonFasterXmlRequest = new DataSaveJsonFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveJsonFasterXmlData(saveJsonFasterXmlRequest));
        @NotNull final DataLoadJsonFasterXmlRequest loadJsonFasterXmlRequest = new DataLoadJsonFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadJsonFasterXmlData(loadJsonFasterXmlRequest));
    }

    @Test
    public void TestJsonJaxB() {
        @NotNull final DataSaveJsonJaxBRequest saveJsonJaxBRequest = new DataSaveJsonJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveJsonJaxBData(saveJsonJaxBRequest));
        @NotNull final DataLoadJsonJaxBRequest loadJsonJaxBRequest = new DataLoadJsonJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadJsonJaxBData(loadJsonJaxBRequest));
    }

    @Test
    public void TestXmlFasterXML() {
        @NotNull final DataSaveXmlFasterXmlRequest saveXmlFasterXmlRequest = new DataSaveXmlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveXmlFasterXmlData(saveXmlFasterXmlRequest));
        @NotNull final DataLoadXmlFasterXmlRequest loadXmlFasterXmlRequest = new DataLoadXmlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadXmlFasterXmlData(loadXmlFasterXmlRequest));
    }

    @Test
    public void TestXmlJaxB() {
        @NotNull final DataSaveXmlJaxBRequest saveXmlJaxBRequest = new DataSaveXmlJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveXmlJaxBData(saveXmlJaxBRequest));
        @NotNull final DataLoadXmlJaxBRequest loadXmlJaxBRequest = new DataLoadXmlJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadXmlJaxBData(loadXmlJaxBRequest));
    }

    @Test
    public void TestYamlFasterXML() {
        @NotNull final DataSaveYamlFasterXmlRequest saveYamlFasterXmlRequest = new DataSaveYamlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.saveYamlFasterXmlData(saveYamlFasterXmlRequest));
        @NotNull final DataLoadYamlFasterXmlRequest loadYamlFasterXmlRequest = new DataLoadYamlFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpoint.loadYamlFasterXmlData(loadYamlFasterXmlRequest));
    }

}