package t1.dkhrunina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.endpoint.IAuthEndpoint;
import t1.dkhrunina.tm.api.endpoint.IUserEndpoint;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.request.user.*;
import t1.dkhrunina.tm.dto.response.user.UserLoginResponse;
import t1.dkhrunina.tm.dto.response.user.UserRegisterResponse;
import t1.dkhrunina.tm.dto.response.user.UserShowProfileResponse;
import t1.dkhrunina.tm.marker.SoapCategory;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.service.PropertyService;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IUserEndpoint userEndpoint;

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private static String newUserToken;

    @Nullable
    private static String userPass = "test";

    @BeforeClass
    public static void initUser() {
        @NotNull IPropertyService propertyService = new PropertyService();
        authEndpoint = IAuthEndpoint.newInstance(propertyService);
        userEndpoint = IUserEndpoint.newInstance(propertyService);
        @NotNull UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();

        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest("test", "test", "test@test.test");
        userEndpoint.registerUser(registerRequest);
        loginRequest = new UserLoginRequest("test", userPass);
        response = authEndpoint.loginUser(loginRequest);
        userToken = response.getToken();
    }

    @AfterClass
    public static void dropUser() {
        @NotNull UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, "test");
        userEndpoint.removeUser(removeRequest);
        if (newUserToken == null) return;
        removeRequest = new UserRemoveRequest(adminToken, "new");
        userEndpoint.removeUser(removeRequest);
    }

    @Test
    public void testChangePassword() {
        @NotNull final String newPass = "newPass";
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(userToken, newPass);
        Assert.assertNotNull(userEndpoint.changeUserPassword(request));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("test", newPass);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.loginUser(loginRequest);
        Assert.assertNotNull(loginResponse);
        userToken = loginResponse.getToken();
        Assert.assertNotNull(userToken);
        Assert.assertFalse(userToken.isEmpty());
        userPass = newPass;
    }

    @Test
    public void testChangePasswordTokenNull() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(null, "pass");
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(request));
    }

    @Test
    public void testLockAndUnlock() {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken, "test");
        Assert.assertNotNull(userEndpoint.lockUser(request));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("test", userPass);
        @NotNull final UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        Assert.assertFalse(response.getSuccess());
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken, "test");
        Assert.assertNotNull(userEndpoint.unlockUser(unlockRequest));
        @NotNull final UserLoginRequest newLoginRequest = new UserLoginRequest("test", userPass);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.loginUser(newLoginRequest);
        Assert.assertNotNull(loginResponse);
        userToken = loginResponse.getToken();
        Assert.assertNotNull(userToken);
        Assert.assertFalse(userToken.isEmpty());
    }

    @Test
    public void testLockAdmin() {
        @NotNull final UserLockRequest request = new UserLockRequest(userToken, "admin");
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(request));
    }

    @Test
    public void testUnlockAdmin() {
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(userToken, "admin");
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(unlockRequest));
    }

    @Test
    public void testLogout() {
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(userToken);
        Assert.assertNotNull(authEndpoint.logoutUser(logoutRequest));
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(userToken, userPass);
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(changePasswordRequest));
        @NotNull final UserLoginRequest newLoginRequest = new UserLoginRequest("test", userPass);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.loginUser(newLoginRequest);
        Assert.assertNotNull(loginResponse);
        userToken = loginResponse.getToken();
        Assert.assertNotNull(userToken);
        Assert.assertFalse(userToken.isEmpty());
    }

    @Test
    public void testLogoutTokenNull() {
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(null);
        Assert.assertThrows(Exception.class, () -> authEndpoint.logoutUser(logoutRequest));
    }

    @Test
    public void testRegisterAndRemove() {
        @NotNull final String newUserLogin = "newUser";
        @NotNull final String newUserPassword = "newPass";
        @NotNull final String newUserEmail = "new@new.new";
        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest(newUserLogin, newUserPassword, newUserEmail);
        Assert.assertNotNull(userEndpoint.registerUser(registerRequest));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(newUserLogin, newUserPassword);
        @Nullable final UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        Assert.assertNotNull(response);
        newUserToken = response.getToken();
        Assert.assertNotNull(newUserToken);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, newUserLogin);
        Assert.assertNotNull(userEndpoint.removeUser(removeRequest));
        @NotNull final UserLoginRequest loginRemovedUserRequest = new UserLoginRequest(newUserLogin, newUserPassword);
        @NotNull final UserLoginResponse removedUserResponse = authEndpoint.loginUser(loginRemovedUserRequest);
        Assert.assertFalse(removedUserResponse.getSuccess());
        newUserToken = null;
    }

    @Test
    public void testRegisterLoginExists() {
        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest("admin", "newPass", "admin@admin.admin");
        @NotNull final UserRegisterResponse response = userEndpoint.registerUser(registerRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    public void testRegisterEmailExists() {
        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest("newUser", "newPass", "test@test.test");
        @NotNull final UserRegisterResponse response = userEndpoint.registerUser(registerRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    public void testRegisterLoginEmpty() {
        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest("", "newPass", "new@new.new");
        @NotNull final UserRegisterResponse response = userEndpoint.registerUser(registerRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    public void testRegisterPasswordEmpty() {
        @NotNull final UserRegisterRequest registerRequest = new UserRegisterRequest("newUser", "", "new@new.new");
        @NotNull final UserRegisterResponse response = userEndpoint.registerUser(registerRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    public void testRemoveAdmin() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(userToken, "admin");
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(removeRequest));
    }

    @Test
    public void testUpdateProfile() {
        @NotNull final String newFirstName = "First";
        @NotNull final String newLastName = "Last";
        @NotNull final String newMiddleName = "Middle";
        @NotNull final UserUpdateProfileRequest updateRequest = new UserUpdateProfileRequest(userToken, newFirstName, newMiddleName, newLastName);
        Assert.assertNotNull(userEndpoint.updateUserProfile(updateRequest));
        @NotNull final UserShowProfileRequest showProfileRequest = new UserShowProfileRequest(userToken);
        @Nullable final UserShowProfileResponse response = authEndpoint.showUserProfile(showProfileRequest);
        Assert.assertNotNull(response);
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
    }

    @Test
    public void testUpdateProfileTokenNull() {
        @NotNull final UserUpdateProfileRequest updateRequest = new UserUpdateProfileRequest(null, "First", "Last", "Middle");
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(updateRequest));
    }

    @Test
    public void testShowProfile() {
        @NotNull final UserShowProfileRequest showProfileRequest = new UserShowProfileRequest(userToken);
        @Nullable final UserShowProfileResponse response = authEndpoint.showUserProfile(showProfileRequest);
        Assert.assertNotNull(response);
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user);
    }

    @Test
    public void testShowProfileTokenNull() {
        @NotNull final UserShowProfileRequest showProfileRequest = new UserShowProfileRequest(null);
        Assert.assertThrows(Exception.class, () -> authEndpoint.showUserProfile(showProfileRequest));
    }

}