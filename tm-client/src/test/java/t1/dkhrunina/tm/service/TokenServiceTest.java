package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import t1.dkhrunina.tm.api.service.ITokenService;
import t1.dkhrunina.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class TokenServiceTest {

    @Test
    public void getAndSetToken() {
        @NotNull final String testToken = "newToken";
        @NotNull final ITokenService tokenService = new TokenService();
        tokenService.setToken(testToken);
        Assert.assertEquals(testToken, tokenService.getToken());
    }

}