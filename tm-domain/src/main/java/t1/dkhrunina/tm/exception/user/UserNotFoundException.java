package t1.dkhrunina.tm.exception.user;

public class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error: user not found");
    }

}