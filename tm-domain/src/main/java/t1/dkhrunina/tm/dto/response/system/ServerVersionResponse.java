package t1.dkhrunina.tm.dto.response.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import t1.dkhrunina.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class ServerVersionResponse extends AbstractResponse {

    private String version;

}