package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskCreateResponse extends AbstractResultResponse {

    @NotNull
    private TaskDTO task;

    public TaskCreateResponse(@NotNull final TaskDTO task) {
        this.task = task;
    }

    public TaskCreateResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}