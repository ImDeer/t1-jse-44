package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearResponse extends AbstractResultResponse {

    public TaskClearResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}