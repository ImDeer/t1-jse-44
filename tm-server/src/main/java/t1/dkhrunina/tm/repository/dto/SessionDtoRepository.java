package t1.dkhrunina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.api.repository.dto.ISessionDtoRepository;
import t1.dkhrunina.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO>
        implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<SessionDTO> getEntityClass() {
        return SessionDTO.class;
    }

}