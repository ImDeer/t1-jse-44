package t1.dkhrunina.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear();

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @Nullable String projectId);

    @Nullable
    List<Task> findAll();

    @NotNull Collection<Task> set(@NotNull Collection<Task> tasks);

    void update(@NotNull Task task);

    void removeAllByProjectId(@NotNull String userId, @Nullable String projectId);

    @Nullable
    @SuppressWarnings("rawtypes")
    List<Task> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    List<Task> findAll(@NotNull String userId, @Nullable Sort sort);

    @NotNull
    Task updateById(@NotNull String userId, @Nullable String id, @Nullable String name,
                    @Nullable String description);

    @NotNull
    Task updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name,
                       @Nullable String description);

}