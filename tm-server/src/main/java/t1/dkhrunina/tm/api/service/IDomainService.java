package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.Domain;

public interface IDomainService {

    Domain getDomain() throws Exception;

    void setDomain(@Nullable Domain domain) throws Exception;

    void loadDataBackup();

    void loadDataBase64();

    void loadDataBinary();

    void loadDataJsonFasterXml();

    void loadDataJsonJaxB();

    void loadDataXmlFasterXml();

    void loadDataXmlJaxB();

    void loadDataYamlFasterXml();

    void saveDataBackup();

    void saveDataBase64();

    void saveDataBinary();

    void saveDataJsonFasterXml();

    void saveDataJsonJaxB();

    void saveDataXmlFasterXml();

    void saveDataXmlJaxB();

    void saveDataYamlFasterXml();

}