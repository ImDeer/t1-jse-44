package t1.dkhrunina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.repository.dto.ISessionDtoRepository;
import t1.dkhrunina.tm.api.repository.dto.IUserDtoRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.SessionDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SessionDtoRepositoryTest {

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private static EntityManager entityManager;

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private String USER1_ID;

    @NotNull
    private String USER2_ID;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private ISessionDtoRepository sessionRepository;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        @NotNull final UserDTO user1 = new UserDTO();
        USER1_ID = user1.getId();
        @NotNull final UserDTO user2 = new UserDTO();
        USER2_ID = user2.getId();
        userRepository.add(user1);
        userRepository.add(user2);
        sessionRepository = new SessionDtoRepository(entityManager);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            if (i < 5) session.setUserId(USER1_ID);
            else session.setUserId(USER2_ID);
            session.setRole(Role.USUAL);
            sessionList.add(session);
            sessionRepository.add(session);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setRole(Role.USUAL);
        sessionRepository.add(USER1_ID, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final SessionDTO createdSession = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(USER1_ID, createdSession.getUserId());
    }

    @Test
    public void testAddNull() {
        @Nullable final SessionDTO createdSessionDTO = sessionRepository.add(USER1_ID, null);
        Assert.assertNull(createdSessionDTO);
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 2;
        @NotNull final List<SessionDTO> sessions = new ArrayList<>();
        @NotNull final SessionDTO firstSessionDTO = new SessionDTO();
        firstSessionDTO.setRole(Role.USUAL);
        sessions.add(firstSessionDTO);
        @NotNull final SessionDTO secondSessionDTO = new SessionDTO();
        secondSessionDTO.setRole(Role.USUAL);
        sessions.add(secondSessionDTO);
        @NotNull final Collection<SessionDTO> addedSessionDTOs = sessionRepository.add(sessions);
        Assert.assertTrue(addedSessionDTOs.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear(USER1_ID);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER1_ID));
    }

    @Test
    public void testFindAll() {
        @Nullable final List<SessionDTO> sessions = sessionRepository.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertTrue(sessions.size() > 0);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<SessionDTO> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<SessionDTO> sessions = sessionRepository.findAll(USER1_ID);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionListForUser.size(), sessions.size());
    }

    @Test
    public void testFindOneById() {
        @Nullable SessionDTO session;
        for (int i = 0; i < sessionList.size(); i++) {
            session = sessionList.get(i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById(sessionId);
            Assert.assertNotNull(foundSessionDTO);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById("meow");
        Assert.assertNull(foundSessionDTO);
        @Nullable final SessionDTO foundSessionDTONull = sessionRepository.findOneById(null);
        Assert.assertNull(foundSessionDTONull);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable SessionDTO session;
        @NotNull List<SessionDTO> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionListForUser.size(); i++) {
            session = sessionRepository.findOneByIndex(USER1_ID, i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById(USER1_ID, sessionId);
            Assert.assertNotNull(foundSessionDTO);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final SessionDTO foundSessionDTO = sessionRepository.findOneById(USER1_ID, "meow");
        Assert.assertNull(foundSessionDTO);
        @Nullable final SessionDTO foundSessionDTONull = sessionRepository.findOneById(USER1_ID, null);
        Assert.assertNull(foundSessionDTONull);
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(1);
        Assert.assertNotNull(session);
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(null);
        Assert.assertNull(session);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull List<SessionDTO> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionListForUser.size(); i++) {
            @Nullable final SessionDTO session = sessionRepository.findOneByIndex(USER1_ID, i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        @Nullable final SessionDTO session = sessionRepository.findOneByIndex(USER1_ID, null);
        Assert.assertNull(session);
    }

    @Test
    public void testGetSize() {
        int actualSize = sessionRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .count();
        int actualSize = sessionRepository.getSize(USER1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final SessionDTO session = sessionList.get(1);
        Assert.assertNotNull(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final SessionDTO deletedSessionDTO = sessionRepository.remove(session);
        Assert.assertNotNull(deletedSessionDTO);
        @Nullable final SessionDTO deletedSessionDTOInRepository = sessionRepository.findOneById(sessionId);
        Assert.assertNull(deletedSessionDTOInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final SessionDTO session = sessionRepository.remove(null);
        Assert.assertNull(session);
    }

}