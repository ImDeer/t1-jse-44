package t1.dkhrunina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import t1.dkhrunina.tm.api.repository.dto.IProjectDtoRepository;
import t1.dkhrunina.tm.api.repository.dto.ITaskDtoRepository;
import t1.dkhrunina.tm.api.repository.dto.IUserDtoRepository;
import t1.dkhrunina.tm.api.service.IConnectionService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.dto.model.ProjectDTO;
import t1.dkhrunina.tm.dto.model.TaskDTO;
import t1.dkhrunina.tm.dto.model.UserDTO;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.service.ConnectionService;
import t1.dkhrunina.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskDtoRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private String USER1_ID;

    @NotNull
    private String USER2_ID;

    @NotNull
    private String PROJECT1_ID;

    @NotNull
    private String PROJECT2_ID;

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private ITaskDtoRepository taskRepository;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        @NotNull final UserDTO user1 = new UserDTO();
        USER1_ID = user1.getId();
        @NotNull final UserDTO user2 = new UserDTO();
        USER2_ID = user2.getId();
        userRepository.add(user1);
        userRepository.add(user2);
        @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDTO project1 = new ProjectDTO();
        PROJECT1_ID = project1.getId();
        @NotNull final ProjectDTO project2 = new ProjectDTO();
        PROJECT2_ID = project2.getId();
        projectRepository.add(USER1_ID, project1);
        projectRepository.add(USER2_ID, project2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("task " + i);
            task.setDescription("descr " + i);
            if (i < 5) {
                task.setUserId(USER1_ID);
                task.setProjectId(PROJECT1_ID);
            } else {
                task.setUserId(USER2_ID);
                task.setProjectId(PROJECT2_ID);
            }
            taskList.add(task);
            taskRepository.add(task);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String taskName = "test name";
        @NotNull final String taskDescription = "test descr";
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(USER1_ID, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final TaskDTO createdTask = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(USER1_ID, createdTask.getUserId());
        Assert.assertEquals(taskName, createdTask.getName());
        Assert.assertEquals(taskDescription, createdTask.getDescription());
    }

    @Test
    public void testAddNull() {
        @Nullable final TaskDTO createdTaskDTO = taskRepository.add(USER1_ID, null);
        Assert.assertNull(createdTaskDTO);
    }

    @Test
    public void testAddAll() {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        @NotNull final String firstTaskDTOName = "tdto name 1";
        @NotNull final String firstTaskDTODescription = "tdto descr 1";
        @NotNull final TaskDTO firstTaskDTO = new TaskDTO();
        firstTaskDTO.setName(firstTaskDTOName);
        firstTaskDTO.setDescription(firstTaskDTODescription);
        tasks.add(firstTaskDTO);
        @NotNull final String secondTaskDTOName = "tdto name 2";
        @NotNull final String secondTaskDTODescription = "tdto descr 2";
        @NotNull final TaskDTO secondTaskDTO = new TaskDTO();
        secondTaskDTO.setName(secondTaskDTOName);
        secondTaskDTO.setDescription(secondTaskDTODescription);
        tasks.add(secondTaskDTO);
        @NotNull final Collection<TaskDTO> addedTasks = taskRepository.add(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(USER1_ID);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER1_ID));
    }

    @Test
    public void testFindAll() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull List<TaskDTO> taskListForProject = taskList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .filter(m -> PROJECT1_ID.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(USER1_ID, PROJECT1_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForProject, tasks);
    }

    @Test
    public void testFindAllWithComparator() {
        @NotNull Comparator<TaskDTO> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = taskRepository.findAll().size();
        @NotNull List<TaskDTO> tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll(USER1_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    public void testFindAllWithComparatorFor() {
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .filter(m -> PROJECT1_ID.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull Comparator<TaskDTO> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<TaskDTO> tasks = taskRepository.findAll(USER1_ID, comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(USER1_ID, comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(USER1_ID, comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    public void testFindOneById() {
        @Nullable TaskDTO task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTaskDTO);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById("meow");
        Assert.assertNull(foundTaskDTO);
        @Nullable final TaskDTO foundTaskDTONull = taskRepository.findOneById(null);
        Assert.assertNull(foundTaskDTONull);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable TaskDTO task;
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            task = taskListForUser.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById(USER1_ID, taskId);
            Assert.assertNotNull(foundTaskDTO);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById(USER1_ID, "meow");
        Assert.assertNull(foundTaskDTO);
        @Nullable final TaskDTO foundTaskDTONull = taskRepository.findOneById(USER1_ID, null);
        Assert.assertNull(foundTaskDTONull);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 1; i <= taskList.size(); i++) {
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexNull() {
        @Nullable final TaskDTO task = taskRepository.findOneByIndex(null);
        Assert.assertNull(task);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= taskListForUser.size(); i++) {
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(USER1_ID, i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        @Nullable final TaskDTO task = taskRepository.findOneByIndex(USER1_ID, null);
        Assert.assertNull(task);
    }

    @Test
    public void testGetSize() {
        int actualSize = taskRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .count();
        int actualSize = taskRepository.getSize(USER1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final TaskDTO task = taskList.get(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final TaskDTO deletedTaskDTO = taskRepository.remove(task);
        Assert.assertNotNull(deletedTaskDTO);
        @Nullable final TaskDTO deletedTaskDTOInRepository = taskRepository.findOneById(taskId);
        Assert.assertNull(deletedTaskDTOInRepository);
    }

    @Test
    public void testRemoveNull() {
        @Nullable final TaskDTO task = taskRepository.remove(null);
        Assert.assertNull(task);
    }

}